package Humeur;

public class Heureuse implements Humeur{

	private final String COMMENTAIRE = "J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!";
	private final String HUMEUR = "heureuse";

	
	@Override
	public String getComment() {
		return COMMENTAIRE;
	}

	@Override
	public String toString() {
		return HUMEUR;
	}
}
