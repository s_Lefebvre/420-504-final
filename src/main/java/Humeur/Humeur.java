package Humeur;

public interface Humeur {
	
	public String getComment();	
	public String toString();

}

/*
 * Merci d'avoir eu la patience de nous esseign� pendant cette session,
 * j'esp�re que ce fut une exp�rience enrichissante autant pour nous que pour vous.
 * 
 * j'ai beaucoup appr�ci� tous les efforts que vous avez fait afin de rendre votre cour
 * le plus int�ressant et le plus �ducatif possible.
 * 
 * passez un joyeux temps des f�te et encore une fois, merci!
 * 
 * votre �tudiant,
 * s�bastien
 * 
 */
