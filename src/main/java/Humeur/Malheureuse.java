package Humeur;

public class Malheureuse implements Humeur{
	
	private final String COMMENTAIRE = "J'ai besoin d'un MacBook Pro pour être une personne heureuse!";
	private final String HUMEUR = "malheureuse";

	@Override
	public String getComment() {
		return COMMENTAIRE;
	}

	@Override
	public String toString() {
		return HUMEUR;
	}
}
