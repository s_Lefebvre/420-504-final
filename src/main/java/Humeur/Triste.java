package Humeur;

public class Triste implements Humeur{
	
	private final String COMMENTAIRE = "Je fais parti des gens qui n'auront jamais de MacBook Pro";
	private final String HUMEUR = "triste";

	@Override
	public String getComment() {
		return COMMENTAIRE;
	}
	
	@Override
	public String toString() {
		return HUMEUR;
	}

}
