package ca.claurendeau.examen504final;

import Humeur.Humeur;

/**
 * 
 * Remanier le code ci-dessous de façon à ce qu'il utilise le 'State/Strategy Pattern'
 * 
 * Vous devrez nommer chacun des remaniements que vous faites ainsi que de faire un commit à chaque remaniement.
 *
 */
public class Personne {
    
    private Humeur humeur;

    public Personne(Humeur humeur) {
        this.humeur = humeur;
    }

    @Override
    public String toString() {
        return "Personne [humeur=" + humeur + "]";
    }

    public Humeur getHumeur() {
        return humeur;
    }
    
}
