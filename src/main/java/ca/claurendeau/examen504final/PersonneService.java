package ca.claurendeau.examen504final;

import java.util.ArrayList;
import java.util.List;

import Humeur.Heureuse;
import Humeur.Malheureuse;
import Humeur.Triste;

public class PersonneService {
    
    public static void main(String[] args) {
        
        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne(new Heureuse()));
        personnes.add(new Personne(new Malheureuse()));
        personnes.add(new Personne(new Triste()));
        
        personnes.stream()
                 .forEach(System.out::println);
        
        for (Personne personne : personnes) {
            System.out.println(personne.getHumeur().getComment());
        }
    }
}
