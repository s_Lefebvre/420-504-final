package ca.claurendeau.examen504final;

import static org.junit.Assert.*;

import org.junit.Test;

import Humeur.Heureuse;
import Humeur.Malheureuse;
import Humeur.Triste;

public class PersonneServiceTest {

	@Test
	public void testComment() {
		assertTrue(new Triste().getComment().equals("Je fais parti des gens qui n'auront jamais de MacBook Pro"));
		assertTrue(new Heureuse().getComment().equals("J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!"));
		assertTrue(new Malheureuse().getComment().equals("J'ai besoin d'un MacBook Pro pour être une personne heureuse!"));
	}

}
