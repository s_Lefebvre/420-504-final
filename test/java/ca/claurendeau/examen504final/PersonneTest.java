package ca.claurendeau.examen504final;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import Humeur.Heureuse;
import Humeur.Humeur;
import Humeur.Malheureuse;
import Humeur.Triste;

public class PersonneTest {
	public Personne personne1;
	public Personne personne2;
	public Personne personne3;
	
	@Before
	public void setUp() {
		personne1 = new Personne(new Malheureuse());
		personne2 = new Personne(new Triste());
		personne3 = new Personne(new Heureuse());
	}
    
    @Test
    public void testGetHumeur() {
    	assertTrue(personne1.getHumeur().toString().equals(new Malheureuse().toString())); 
    	assertTrue(personne2.getHumeur().toString().equals(new Triste().toString())); 
    	assertTrue(personne3.getHumeur().toString().equals(new Heureuse().toString())); 
    }

}
